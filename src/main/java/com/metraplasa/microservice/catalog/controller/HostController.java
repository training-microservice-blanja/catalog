package com.metraplasa.microservice.catalog.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HostController {

	@GetMapping("/api/host")
	public Map<String, Object> hostInfo(HttpServletRequest httpServletRequest) {
		Map<String, Object> info = new HashMap<>();
		info.put("host", httpServletRequest.getLocalName());
		info.put("address", httpServletRequest.getLocalAddr());
		info.put("port", httpServletRequest.getLocalPort());
		return info;
	}
}
