package com.metraplasa.microservice.catalog.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.metraplasa.microservice.catalog.dao.ProductDao;
import com.metraplasa.microservice.catalog.entity.Product;

@RestController
public class ProductController {

	@Autowired
	private ProductDao productDao;
	
	@GetMapping("/api/product")
	public Iterable<Product> allProducts() {
		return productDao.findAll();
	}
}
