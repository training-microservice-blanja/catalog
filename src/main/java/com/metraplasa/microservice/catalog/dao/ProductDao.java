package com.metraplasa.microservice.catalog.dao;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.metraplasa.microservice.catalog.entity.Product;

public interface ProductDao extends PagingAndSortingRepository<Product, String>{

}
